#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *arr[] = {"false", "1", "0", "1", "2", "0", "1", "3", "a"};
int arrSize = sizeof(arr) / sizeof(arr[0]);

void sort(){
    int i, j, temp = 0;

    for(i = 0; i < arrSize; i++){
        if(strcmp(arr[i], "0") == 0){
            for(j = i; j < arrSize-1; j++){
                arr[j] = arr[j + 1];
            }
        }
    }

    for(i = arrSize - 1; i > arrSize - 3; i--)
        arr[i] = "0";
}

void main(){
    int i;

    printf("Sifirlari sona atmadan once-\n");
    for(i = 0; i < arrSize; i++)
        printf("arr[%d]: %s\n", i, arr[i]);

    sort();

    printf("\nSifirlari sona attiktan sonra-\n");
    for(i = 0; i < arrSize; i++)
        printf("arr[%d]: %s\n", i, arr[i]);
}